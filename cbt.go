package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/sathias/cbt/event_processor"
	dbLib "gitlab.com/sathias/cbt/internal/db"
	"gitlab.com/sathias/cbt/internal/event_reader"
	"gitlab.com/sathias/cbt/services"
	"time"
)

func main() {
	//Tomorrow event reader could be from kafka or other stream as well.
	consumer := event_reader.NewSimpleJSONFileReader("test.jsonl")
	eventHandler := event_processor.NewEventProcessor()
	db := dbLib.NewDB()
	ctx := context.TODO()

	eventHandler.Register(ctx, event_processor.NewCartEventProcessor(db))
	definedProductAggregators := services.NewAggregator(db)

	for {
		event, err := consumer.ReadMessage(ctx)
		if err != nil && !errors.Is(err, event_reader.ErrDataNotFound) {

			break
		}
		if event == nil {
			break
		}

		var rawEvent map[string]interface{}
		json.Unmarshal(event, &rawEvent)

		//Process event will take care of event
		eventHandler.Process(ctx, rawEvent)

	}

	//Max visited product for 120 days
	visitedProduct, _ := definedProductAggregators.MaxVisitedProduct(ctx, 24*time.Hour*120)
	fmt.Println(fmt.Sprintf("Max visited product for last 120 days is %s, total visits: %d", visitedProduct.ItemID, visitedProduct.Count))

	//Highest grossing product for 120 days
	highestGrossingProduct, _ := definedProductAggregators.HighestGrossingProduct(ctx, 24*time.Hour*120)
	fmt.Println(fmt.Sprintf("Highest grosing product for last 120 days is %s, total amount: %.2f", highestGrossingProduct.ItemID, highestGrossingProduct.Amount))

	//Conversion rate calculated for 120 days
	conversionRate, _ := definedProductAggregators.ConversionRate(ctx, 24*time.Hour*120)
	fmt.Println(fmt.Sprintf("Total conversion for last 120 days is %d", conversionRate.Rate))
}
