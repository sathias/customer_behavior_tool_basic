package db

import (
	"context"
	"sort"
	"time"
)

type Table interface{}

type Event interface {
	TimeStamp() time.Time
}

type DB interface {
	SaveEvent(ctx context.Context, TableName string, Row Event) error
	GetEvents(ctx context.Context, table string, start time.Time, end time.Time) []Event
}

type dB struct {
	rows map[string][]Event
}

func NewDB() DB {
	return &dB{}
}

// Save This is basic DB wrapper, this might not work with production DB.
// Save TODO: this implementation does not handle duplicates etc, we could handle duplicates by introducing primary key or idemportent key for each event type.
func (d *dB) SaveEvent(ctx context.Context, table string, row Event) error {
	if len(d.rows) == 0 {
		d.rows = make(map[string][]Event, 0)
	}

	d.rows[table] = append(d.rows[table], row)

	sort.Slice(d.rows[table], func(i, j int) bool {
		return d.rows[table][i].TimeStamp().After(d.rows[table][i].TimeStamp())
	})

	return nil
}

func (d *dB) GetEvents(ctx context.Context, table string, start time.Time, end time.Time) []Event {
	events := make([]Event, 0)
	for _, event := range d.rows[table] {
		if event.TimeStamp().After(start) && event.TimeStamp().Before(end) {
			events = append(events, event)
		}
	}

	return events
}
