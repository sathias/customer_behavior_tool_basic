package event_reader

import (
	"bufio"
	"context"
	"errors"
	"os"
)

var ErrDataNotFound = errors.New("data EOF")

type EventReaderInterface interface {
	ReadMessage(ctx context.Context) ([]byte, error)
}

// simpleJSONFileReader There could be many reader implementations for this program. For instance, we might want to read events from a folder, a large file, or a Kafka stream. This interface must be implemented to introduce new readers.
type simpleJSONFileReader struct {
	filePath   string
	data       []string
	readOffset int
}

func NewSimpleJSONFileReader(filePath string) EventReaderInterface {
	return &simpleJSONFileReader{filePath: filePath, readOffset: 0, data: []string{}}
}

// ReadMessage Data is currently stored in memory, and it is retained. but messages can be flushed once it is read or it can be flushed on every interval.
func (s *simpleJSONFileReader) ReadMessage(ctx context.Context) ([]byte, error) {
	err := s.appendToQueue(ctx)
	if err != nil {
		return nil, err
	}

	if s.readOffset >= len(s.data) {
		return nil, ErrDataNotFound
	}
	s.readOffset += 1
	return []byte(s.data[s.readOffset-1]), nil
}

// TODO: Locking this function has to be done.
func (s *simpleJSONFileReader) appendToQueue(ctx context.Context) error {
	if s.readOffset > 0 {
		return nil
	}

	file, err := os.Open(s.filePath)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s.data = append(s.data, scanner.Text())
	}

	return nil
}
