## How to build and run CBT code
```bash
go build cbt.gogit init --initial-branch=main

./cbt
```

# Go Code Review for Interview Scope

This source code is tested with Go 1.20 and is intended for completion within the scope of an interview. While the code is functional, there are several areas for improvement, primarily related to how data structures are utilized.

## Key Points

1. **Reading from CSV File:**
  - The file is read using the `reader` interface. In the future, a Kafka reader can be implemented to reuse this code with minimal changes.

2. **Event Processing:**
  - The code currently processes e-commerce related events and includes processors for these events. It is designed to be extendable for adding multiple processors to handle different event types.

3. **In-Memory Event Storage:**
  - Events in the CSV file are unordered, so they are stored in an ordered fashion in memory.
  - The current sorting method is inefficient as it sorts on every write. This requires optimization before moving to production.

4. **Event Storage Design:**
  - Each event type is stored in a separate array. This design choice was made to enable parallel queries and simplify querying.

5. **Analytics Queries:**
  - When an analytics query is made, the code sequentially fetches from the array and filters out relevant events for the given start and end time, performing aggregation on the fly. This approach is inefficient and needs optimization.

## Considerations for Improvement

To simplify reads and improve efficiency, consider the following:

1. **Timed Buckets:**
  - Create timed buckets and store aggregates during writes in these buckets, e.g., hourly data.
  - When a read query is performed, recalculate aggregates from these pre-calculated aggregates.

2. Test cases need to be added for all processing

3. By implementing these improvements, the code will be more efficient and ready for production use.


# Production-Grade System for Customer Behavior Data

## Overview

To build a robust system for capturing and analyzing customer behavior data, I recommend the following architecture and tools:

1. **Event Ingestion**
2. **Stream Processing**
3. **Storage**

### Event Ingestion

All events should be directed to Kafka. Various services such as Cart, Storefront, and Payment will produce events to Kafka.

- **Kafka Configuration:**
    - Configure Kafka with a retention policy of around 7 days. This duration helps in retrying in case of any failures.

### Stream Processing

- **Kafka Consumer or Flink Consumer:**
    - Initially, a Kafka consumer can be implemented.
    - Flink can be introduced later for advanced stream processing. Flink facilitates seamless integration with various data sources.

### Storage

For storage, I recommend using Pinot:

- **Pinot:**
    - Pinot is an open-source OLAP system that supports both real-time and historical analytics.
    - Based on research, Pinot is 4x faster than ClickHouse.
    - Pinot offers rich indexing options, REST API support, and seamless Kafka integration.

Other considerations include ClickHouse and Druid, but Pinot is preferred due to its performance and feature set.
