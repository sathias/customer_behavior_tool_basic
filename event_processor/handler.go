package event_processor

import (
	"context"
	"encoding/json"
	"fmt"
	"time"
)

type EventProcessor interface {
	Process(ctx context.Context, event Event)
}

type Event struct {
	EventType  string  `json:"event_type"`
	CustomerID string  `json:"customer_id"`
	ItemID     string  `json:"item_id"`
	Timestamp  string  `json:"timestamp"`
	Price      float64 `json:"price"`
}

func (e Event) TimeStamp() time.Time {
	layout := "2006-01-02T15:04:05.000000"
	formattedTime, err := time.Parse(layout, e.Timestamp)
	if err != nil {
		fmt.Println(err)
		formattedTime = time.Now()
	}
	return formattedTime
}

type EventHandler interface {
	Process(ctx context.Context, rawEvent map[string]interface{}) error
	Register(ctx context.Context, processor EventProcessor)
}

type eventHandler struct {
	eventProcessList []EventProcessor
}

func NewEventProcessor() EventHandler {
	return &eventHandler{}
}

func (p eventHandler) Process(ctx context.Context, rawEvent map[string]interface{}) error {
	event, err := p.rawEventToEvent(rawEvent)
	if err != nil {
		return err
	}

	for _, processor := range p.eventProcessList {
		processor.Process(ctx, event)
	}

	return nil
}

func (p *eventHandler) Register(ctx context.Context, processor EventProcessor) {
	p.eventProcessList = append(p.eventProcessList, processor)
}

func (p eventHandler) rawEventToEvent(rawEvent map[string]interface{}) (Event, error) {
	bytes, err := json.Marshal(rawEvent)
	if err != nil {
		return Event{}, err
	}

	var event Event
	if err = json.Unmarshal(bytes, &event); err != nil {
		return Event{}, err
	}

	return event, nil
}
