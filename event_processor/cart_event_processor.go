package event_processor

import (
	"context"
	"fmt"
	"gitlab.com/sathias/cbt/internal/db"
)

var pageViewEvent = "page_visit"
var purchaseEvent = "purchase"
var addToCart = "add_to_cart"

type CartEventProcessor struct {
	db db.DB
}

func NewCartEventProcessor(db db.DB) EventProcessor {
	return &CartEventProcessor{
		db: db,
	}
}

func (c CartEventProcessor) Process(ctx context.Context, event Event) {

	switch eventType := event.EventType; eventType {
	case pageViewEvent, addToCart, purchaseEvent:
		c.db.SaveEvent(ctx, event.EventType, event)

	default:
		fmt.Println("skipping: ", eventType)
	}
}
