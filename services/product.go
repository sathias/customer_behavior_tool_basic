package services

import (
	"context"
	"gitlab.com/sathias/cbt/event_processor"
	"gitlab.com/sathias/cbt/internal/db"
	"math"
	"time"
)

type MaxVisitedProduct struct {
	ItemID string
	Count  int
}

type HighestGrossingProduct struct {
	ItemID string
	Amount float64
}

type ConversionRate struct {
	Rate int
}

type AggregatorService interface {
	MaxVisitedProduct(ctx context.Context, duration time.Duration) (MaxVisitedProduct, error)
	HighestGrossingProduct(ctx context.Context, duration time.Duration) (HighestGrossingProduct, error)
	ConversionRate(ctx context.Context, duration time.Duration) (ConversionRate, error)
}

type productVisitAggregator struct {
	db db.DB
}

func NewAggregator(db db.DB) AggregatorService {
	return &productVisitAggregator{db: db}
}

// Rather than computing
func (r productVisitAggregator) MaxVisitedProduct(ctx context.Context, duration time.Duration) (MaxVisitedProduct, error) {
	productMap := r.productCountByViews(ctx, duration)

	return r.maxOfProductView(ctx, productMap), nil
}

func (r productVisitAggregator) HighestGrossingProduct(ctx context.Context, duration time.Duration) (HighestGrossingProduct, error) {
	productMap := r.orderCountByProductID(ctx, duration)
	return r.maxOfProductViewForOrder(ctx, productMap), nil
}

func (r productVisitAggregator) ConversionRate(ctx context.Context, duration time.Duration) (ConversionRate, error) {
	pageViewEvents := r.totalPageViewEvents(ctx, duration)
	purchaseEvents := r.totalPurchaseEvents(ctx, duration)
	if purchaseEvents == 0 {
		return ConversionRate{Rate: 0}, nil
	}

	rate := int(math.Floor((float64(purchaseEvents) / float64(pageViewEvents)) * 100))
	return ConversionRate{Rate: rate}, nil
}

func (r productVisitAggregator) productCountByViews(ctx context.Context, duration time.Duration) map[string]int {
	productMap := make(map[string]int, 0)

	//As this is sorted array instead of querying all events, we can query based on timestamp. BST could be used to run this query efficiently
	events := r.db.GetEvents(ctx, "page_visit", time.Now().Add(-duration), time.Now())
	for _, p := range events {
		event, ok := p.(event_processor.Event)
		if ok {
			if productMap[event.ItemID] > 0 {
				productMap[event.ItemID] += 1
			} else {
				productMap[event.ItemID] = 1
			}
		}
	}

	return productMap
}

func (r productVisitAggregator) orderCountByProductID(ctx context.Context, duration time.Duration) map[string]int {
	productMap := make(map[string]int, 0)

	//As this is sorted array instead of querying all events, we can query based on timestamp. BST could be used to run this query efficiently
	events := r.db.GetEvents(ctx, "purchase", time.Now().Add(-duration), time.Now())
	for _, p := range events {
		event, ok := p.(event_processor.Event)
		if ok {
			if productMap[event.ItemID] > 0 {
				productMap[event.ItemID] += int(event.Price * 100)
			} else {
				productMap[event.ItemID] = int(event.Price * 100)
			}
		}
	}

	return productMap
}

func (r productVisitAggregator) maxOfProductView(ctx context.Context, productMap map[string]int) MaxVisitedProduct {
	maxProductCount := 0
	var productView MaxVisitedProduct

	for id, v := range productMap {
		if v > maxProductCount {
			maxProductCount = v
			productView.ItemID = id
			productView.Count = maxProductCount
		}
	}

	return productView
}

func (r productVisitAggregator) maxOfProductViewForOrder(ctx context.Context, productMap map[string]int) HighestGrossingProduct {
	maxProductAmount := 0
	var productView HighestGrossingProduct

	for id, v := range productMap {
		if v > maxProductAmount {
			maxProductAmount = v
			productView.ItemID = id
			productView.Amount = float64(maxProductAmount / 100)
		}
	}

	return productView
}

func (r productVisitAggregator) totalPurchaseEvents(ctx context.Context, duration time.Duration) int {

	//As this is sorted array instead of querying all events, we can query based on timestamp. BST could be used to run this query efficiently
	events := r.db.GetEvents(ctx, "purchase", time.Now().Add(-duration), time.Now())

	return len(events)
}

func (r productVisitAggregator) totalPageViewEvents(ctx context.Context, duration time.Duration) int {

	//As this is sorted array instead of querying all events, we can query based on timestamp. BST could be used to run this query efficiently
	events := r.db.GetEvents(ctx, "page_visit", time.Now().Add(-duration), time.Now())

	return len(events)
}
